#include "HookManager.h"
#include "rightsdef.h"
#include <Shlobj.h>
#include <new>
#include "thirdpart\Detours\include\detours.h"
#ifdef _WIN64 
#pragma comment(lib, "thirdpart/Detours/lib.X64/detours.lib")
#else
#pragma comment(lib, "thirdpart/Detours/lib.X86/detours.lib")
#endif // _WIN64 
#define OLE32DLL "ole32.dll"
#define REGISTER_DRAP_DROP_API "RegisterDragDrop"
#define DO_DRAP_DROP_API "DoDragDrop"

namespace HookManager 
{
	REGISTER_DRAG_DROP_FUN oldRegisterDragDrop = nullptr;
	DO_DRAG_DROP_FUN oldDoDragDrop = nullptr;
	IEventBase* g_rights = nullptr;

	HRESULT WINAPI Core_RegisterDragDrop(IN HWND hwnd, IN LPDROPTARGET pDropTarget)
	{
		HRESULT hr = S_OK;
		CoreIDropTarget *pCoreIDropTarget = NULL;
		IDropTarget *pMyIDropTarget = pDropTarget;
		do
		{
			if (!pDropTarget)
			{
				hr = oldRegisterDragDrop(hwnd, pDropTarget);
				break;
			}
			try
			{
				pCoreIDropTarget = new CoreIDropTarget(pDropTarget);
				pMyIDropTarget = (IDropTarget *)pCoreIDropTarget;
			}
			catch (std::bad_alloc e)
			{
				pMyIDropTarget = pDropTarget;
			}
			hr = oldRegisterDragDrop(hwnd, pMyIDropTarget);
		} while (FALSE);
		return hr;
	}

	HRESULT WINAPI Core_DoDragDrop(IN LPDATAOBJECT pDataObj, IN LPDROPSOURCE pDropSource, IN DWORD dwOKEffects, OUT LPDWORD pdwEffect)
	{
		ULONGLONG activeRights = BUILTIN_RIGHT_ALL;
		if (g_rights)
			g_rights->GetActiveRights(activeRights);
		if (!(activeRights & BUILTIN_RIGHT_EDIT))
		{
			//pdwEffect = DROPEFFECT_NONE;
			return DRAGDROP_S_CANCEL;	// block
		}
		else
		{
			return oldDoDragDrop(pDataObj, pDropSource, dwOKEffects, pdwEffect);
		}
	}

	void hook()
	{
		oldRegisterDragDrop = (REGISTER_DRAG_DROP_FUN)DetourFindFunction(OLE32DLL, REGISTER_DRAP_DROP_API);
		oldDoDragDrop = (DO_DRAG_DROP_FUN)DetourFindFunction(OLE32DLL, DO_DRAP_DROP_API);
		DetourRestoreAfterWith();
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourAttach((void**)&oldRegisterDragDrop, Core_RegisterDragDrop);	// hook RegisterDragDrop
		DetourAttach((void**)&oldDoDragDrop, Core_DoDragDrop);				// hook DoDragDrop
		DetourTransactionCommit();
	}

	void unhook()
	{
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		DetourDetach((void**)&oldRegisterDragDrop, Core_RegisterDragDrop);
		DetourDetach((void**)&oldDoDragDrop, Core_DoDragDrop);
		DetourTransactionCommit();
	}

	void SetEventSink(IEventBase* pPights) { g_rights = pPights; }

	CoreIDropTarget::CoreIDropTarget()
	{
		m_uRefCount = 0;
		m_pIDropTarget = NULL;
	}

	CoreIDropTarget::CoreIDropTarget(IDropTarget *pTarget)
	{
		m_uRefCount = 0;
		pTarget->AddRef();
		m_pIDropTarget = pTarget;
	}

	CoreIDropTarget::~CoreIDropTarget()
	{
		if (m_pIDropTarget)
		{
			m_pIDropTarget->Release();
			m_pIDropTarget = NULL;
		}
	}

	STDMETHODIMP CoreIDropTarget::QueryInterface(
		/* [in] */ __RPC__in REFIID riid,
		/* [annotation][iid_is][out] */
		_COM_Outptr_  void **ppvObject)
	{
		HRESULT hRet = S_OK;
		void *punk = NULL;
		*ppvObject = NULL;
		do
		{
			if (IID_IUnknown == riid)
			{
				punk = (IUnknown *)this;
			}
			else if (IID_IDropTarget == riid)
			{
				punk = (IDropTarget*)this;
			}
			else
			{
				hRet = m_pIDropTarget->QueryInterface(riid, ppvObject);
				break;
			}
			AddRef();
			*ppvObject = punk;
		} while (FALSE);
		return hRet;
	}

	STDMETHODIMP_(ULONG) CoreIDropTarget::AddRef()
	{
		m_uRefCount++;
		return m_uRefCount;
	}

	STDMETHODIMP_(ULONG) CoreIDropTarget::Release()
	{
		ULONG uCount = 0;
		if (m_uRefCount)
			m_uRefCount--;
		uCount = m_uRefCount;
		if (!uCount)
		{
			delete this;
		}
		return uCount;
	}

	STDMETHODIMP CoreIDropTarget::DragEnter(
		/* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
		/* [in] */ DWORD grfKeyState,
		/* [in] */ POINTL pt,
		/* [out][in] */ __RPC__inout DWORD *pdwEffect)
	{
		ULONGLONG activeRights = BUILTIN_RIGHT_ALL;
		if (g_rights)
			g_rights->GetActiveRights(activeRights);
		if (!(activeRights & BUILTIN_RIGHT_EDIT))
		{
			*pdwEffect = DROPEFFECT_NONE;	// block
			return S_OK;
		}
		else
		{
			return m_pIDropTarget->DragEnter(pDataObj, grfKeyState, pt, pdwEffect);
		}
	}

	STDMETHODIMP CoreIDropTarget::DragOver(
		/* [in] */ DWORD grfKeyState,
		/* [in] */ POINTL pt,
		/* [out][in] */ __RPC__inout DWORD *pdwEffect)
	{
		return m_pIDropTarget->DragOver(grfKeyState, pt, pdwEffect);
	}

	STDMETHODIMP CoreIDropTarget::DragLeave()
	{
		return m_pIDropTarget->DragLeave();
	}

	STDMETHODIMP CoreIDropTarget::Drop(
		/* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
		/* [in] */ DWORD grfKeyState,
		/* [in] */ POINTL pt,
		/* [out][in] */ __RPC__inout DWORD *pdwEffect)
	{
		return m_pIDropTarget->Drop(pDataObj, grfKeyState, pt, pdwEffect);
	}
}