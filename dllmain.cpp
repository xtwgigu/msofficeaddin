#include "stdafx.h"
#include "HookManager.h"
#include "Log.h"

HMODULE g_hModule = NULL;
BOOL	g_bIsOffice2016 = FALSE;

static BOOL is_office2016(void);
static PIMAGE_NT_HEADERS RtlImageNtHeader(IN PVOID BaseAddress);

BOOL APIENTRY DllMain(HMODULE hModule,WORD  ul_reason_for_call,LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		::OutputDebugStringW(L"nxrmaddin dllmain \n");
		HookManager::hook();
		g_hModule = hModule;

		theLog.Init(0, 0);

		//g_bIsOffice2016 = is_office2016();
		DisableThreadLibraryCalls(hModule);
		break;

	case DLL_THREAD_ATTACH:

		break;
	case DLL_THREAD_DETACH:

		break;

	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

static BOOL is_office2016(void)
{
	BOOL bRet = FALSE;

	IMAGE_NT_HEADERS		*pNtHdr = NULL;
	IMAGE_OPTIONAL_HEADER	*pOptHdr = NULL;

	HMODULE		hOfficeApp = NULL;

	do
	{
		hOfficeApp = GetModuleHandleW(NULL);

		if (!hOfficeApp)
		{
			break;
		}

		pNtHdr = (IMAGE_NT_HEADERS *)RtlImageNtHeader(hOfficeApp);

		if (!pNtHdr)
		{
			break;
		}

		pOptHdr = &pNtHdr->OptionalHeader;

		if (!pOptHdr)
		{
			break;
		}

		if (pOptHdr->MajorLinkerVersion == 14 && pOptHdr->MinorLinkerVersion == 0)	// office 2016 (14, 0) office 2013(10, 10) office 2010(9, 0)
		{
			bRet = TRUE;
			break;
		}

	} while (FALSE);

	return bRet;
}

static PIMAGE_NT_HEADERS RtlImageNtHeader(IN PVOID BaseAddress)
{
	PIMAGE_NT_HEADERS NtHeader;
	PIMAGE_DOS_HEADER DosHeader = (PIMAGE_DOS_HEADER)BaseAddress;

	if (DosHeader && DosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		return NULL;
	}

	if (DosHeader && DosHeader->e_magic == IMAGE_DOS_SIGNATURE)
	{
		NtHeader = (PIMAGE_NT_HEADERS)((ULONG_PTR)BaseAddress + DosHeader->e_lfanew);

		if (NtHeader->Signature == IMAGE_NT_SIGNATURE)
			return NtHeader;
	}

	return NULL;
}
