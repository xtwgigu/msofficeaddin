// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the NXRMCOREADDIN_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// NXRMCOREADDIN_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.


#define NXRMADDIN_NAME							L"NxlRMAddin"

#define NXRMADDIN_INSTALL_POWERPOINT_ADDIN_KEY	L"Software\\Microsoft\\Office\\PowerPoint\\Addins"
#define NXRMADDIN_INSTALL_WINWORD_ADDIN_KEY		L"Software\\Microsoft\\Office\\Word\\Addins"
#define NXRMADDIN_INSTALL_EXCEL_ADDIN_KEY		L"Software\\Microsoft\\Office\\Excel\\Addins"
#define NXRMADDIN_INSTALL_FRIENDLYNAME_VALUE	L"FriendlyName"
#define NXRMADDIN_INSTALL_LOADBEHAVIOR_VALUE	L"LoadBehavior"
#define NXRMADDIN_INSTALL_DESCRIPTION_VALUE		L"Description"

class Inxrmaddin : public IClassFactory
{
public:
	Inxrmaddin();
	~Inxrmaddin();

	STDMETHODIMP QueryInterface(REFIID riid, void **ppobj);

	STDMETHODIMP_(ULONG) AddRef();

	STDMETHODIMP_(ULONG) Release();

	STDMETHODIMP CreateInstance(IUnknown * pUnkOuter, REFIID riid, void ** ppvObject);

	STDMETHODIMP LockServer(BOOL fLock);

private:
	ULONG				m_uRefCount;
	ULONG				m_uLockCount;
};

