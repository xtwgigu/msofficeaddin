#pragma once

#include "import/mso2016.tlh"
#include "import/msppt2016.tlh"
#include "IEventBase.h"

class PowerPointEventListener : public PowerPoint2016::EApplication, public IEventBase
{
public:
	PowerPointEventListener();
	
	PowerPointEventListener(IDispatch *pRibbonUI, BSTR ActiveDoc, ULONGLONG &ActiveRights);

	~PowerPointEventListener();

	HRESULT STDMETHODCALLTYPE QueryInterface( 
		/* [in] */ REFIID riid,
		/* [annotation][iid_is][out] */ 
		_COM_Outptr_  void **ppvObject);

	ULONG STDMETHODCALLTYPE AddRef(void);

	ULONG STDMETHODCALLTYPE Release(void);

	HRESULT STDMETHODCALLTYPE GetTypeInfoCount( 
		/* [out] */ __RPC__out UINT *pctinfo);

	HRESULT STDMETHODCALLTYPE GetTypeInfo( 
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ __RPC__deref_out_opt ITypeInfo **ppTInfo);

	HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
		/* [in] */ __RPC__in REFIID riid,
		/* [size_is][in] */ __RPC__in_ecount_full(cNames) LPOLESTR *rgszNames,
		/* [range][in] */ __RPC__in_range(0,16384) UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ __RPC__out_ecount_full(cNames) DISPID *rgDispId);

	HRESULT STDMETHODCALLTYPE Invoke( 
		/* [annotation][in] */ 
		_In_  DISPID dispIdMember,
		/* [annotation][in] */ 
		_In_  REFIID riid,
		/* [annotation][in] */ 
		_In_  LCID lcid,
		/* [annotation][in] */ 
		_In_  WORD wFlags,
		/* [annotation][out][in] */ 
		_In_  DISPPARAMS *pDispParams,
		/* [annotation][out] */ 
		_Out_opt_  VARIANT *pVarResult,
		/* [annotation][out] */ 
		_Out_opt_  EXCEPINFO *pExcepInfo,
		/* [annotation][out] */ 
		_Out_opt_  UINT *puArgErr);

	HRESULT STDMETHODCALLTYPE WindowSelectionChange(
	/*[in]*/ struct PowerPoint2016::Selection * Sel);
	HRESULT STDMETHODCALLTYPE WindowBeforeRightClick(
	/*[in]*/ struct PowerPoint2016::Selection * Sel,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE WindowBeforeDoubleClick(
	/*[in]*/ struct PowerPoint2016::Selection * Sel,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE PresentationClose(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE PresentationSave(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE PresentationOpen(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE NewPresentation(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE PresentationNewSlide(
	/*[in]*/ struct PowerPoint2016::_Slide * Sld);
	HRESULT STDMETHODCALLTYPE WindowActivate(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres,
	/*[in]*/ struct PowerPoint2016::DocumentWindow * Wn);
	HRESULT STDMETHODCALLTYPE WindowDeactivate(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres,
	/*[in]*/ struct PowerPoint2016::DocumentWindow * Wn);
	HRESULT STDMETHODCALLTYPE SlideShowBegin(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn);
	HRESULT STDMETHODCALLTYPE SlideShowNextBuild(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn);
	HRESULT STDMETHODCALLTYPE SlideShowNextSlide(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn);
	HRESULT STDMETHODCALLTYPE SlideShowEnd(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE PresentationPrint(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE SlideSelectionChanged(
	/*[in]*/ struct PowerPoint2016::SlideRange * SldRange);
	HRESULT STDMETHODCALLTYPE ColorSchemeChanged(
	/*[in]*/ struct PowerPoint2016::SlideRange * SldRange);
	HRESULT STDMETHODCALLTYPE PresentationBeforeSave(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE SlideShowNextClick(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn,
	/*[in]*/ struct PowerPoint2016::Effect * nEffect);
	HRESULT STDMETHODCALLTYPE AfterNewPresentation(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE AfterPresentationOpen(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE PresentationSync(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres,
	/*[in]*/ enum Office2016::MsoSyncEventType SyncEventType);
	HRESULT STDMETHODCALLTYPE SlideShowOnNext(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn);
	HRESULT STDMETHODCALLTYPE SlideShowOnPrevious(
	/*[in]*/ struct PowerPoint2016::SlideShowWindow * Wn);
	HRESULT STDMETHODCALLTYPE PresentationBeforeClose(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE ProtectedViewWindowOpen(
	/*[in]*/ struct PowerPoint2016::ProtectedViewWindow * ProtViewWindow);
	HRESULT STDMETHODCALLTYPE ProtectedViewWindowBeforeEdit(
	/*[in]*/ struct PowerPoint2016::ProtectedViewWindow * ProtViewWindow,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE ProtectedViewWindowBeforeClose(
	/*[in]*/ struct PowerPoint2016::ProtectedViewWindow * ProtViewWindow,
	/*[in]*/ enum PowerPoint2016::PpProtectedViewCloseReason ProtectedViewCloseReason,
	/*[in,out]*/ VARIANT_BOOL * Cancel);
	HRESULT STDMETHODCALLTYPE ProtectedViewWindowActivate(
	/*[in]*/ struct PowerPoint2016::ProtectedViewWindow * ProtViewWindow);
	HRESULT STDMETHODCALLTYPE ProtectedViewWindowDeactivate(
	/*[in]*/ struct PowerPoint2016::ProtectedViewWindow * ProtViewWindow);
	HRESULT STDMETHODCALLTYPE PresentationCloseFinal(
	/*[in]*/ struct PowerPoint2016::_Presentation * Pres);
	HRESULT STDMETHODCALLTYPE AfterDragDropOnSlide(
	/*[in]*/ struct PowerPoint2016::_Slide * Sld,
	/*[in]*/ float X,
	/*[in]*/ float Y);
	HRESULT STDMETHODCALLTYPE AfterShapeSizeChange(
	/*[in]*/ struct PowerPoint2016::Shape * shp);


	STDMETHODIMP GetActiveDoc(
		/*[in,out]*/ std::wstring &ActiveDoc);

	STDMETHODIMP GetActiveRights(
		/*[in,out]*/ ULONGLONG &ActiveRights);

	HRESULT RefreshActiveRights(void);

	void Init(IDispatch *pRibbonUI, BSTR ActiveDoc, ULONGLONG &ActiveRights)
	{
		m_pRibbonUI = pRibbonUI;
		m_ActiveDoc = ActiveDoc;
		m_ActiveDocRights = ActiveRights;
	}

private:
	ULONG				m_uRefCount;

	IDispatch			*m_pRibbonUI;
	
	//NX::utility::CRwLock	m_ActiveDocLock;

	std::wstring		m_ActiveDoc;

	ULONGLONG			m_ActiveDocRights;

	ULONG				m_InvalidCount;

	typedef enum _PowerPointAppEventId {

		WindowSelectionChange_Id = 2001,
		WindowBeforeRightClick_Id,
		WindowBeforeDoubleClick_Id,
		PresentationClose_Id,
		PresentationSave_Id,
		PresentationOpen_Id,
		NewPresentation_Id,
		PresentationNewSlide_Id,
		WindowActivate_Id,
		WindowDeactivate_Id,
		SlideShowBegin_Id,
		SlideShowNextBuild_Id,
		SlideShowNextSlide_Id,
		SlideShowEnd_Id,
		PresentationPrint_Id,
		SlideSelectionChanged_Id,
		ColorSchemeChanged_Id,
		PresentationBeforeSave_Id,
		SlideShowNextClick_Id,
		AfterNewPresentation_Id,
		AfterPresentationOpen_Id,
		PresentationSync_Id,
		SlideShowOnNext_Id,
		SlideShowOnPrevious_Id,
		PresentationBeforeClose_Id,
		ProtectedViewWindowOpen_Id,
		ProtectedViewWindowBeforeEdit_Id,
		ProtectedViewWindowBeforeClose_Id,
		ProtectedViewWindowActivate_Id,
		ProtectedViewWindowDeactivate_Id,
		PresentationCloseFinal_Id,
		AfterDragDropOnSlide_Id,
		AfterShapeSizeChange_Id

	}PowerPointAppEventId;

	void InvalidMsoControls(void);
};
