#include "stdafx.h"
#include "CommonFunction.h"
#include <windows.h>
#include <atlbase.h>
#include <Shlobj.h>
#include <objbase.h>

CommonFunction::CommonFunction()
{
}


CommonFunction::~CommonFunction()
{
}

std::string CommonFunction::WStringToString(const std::wstring& wstr)
{
	std::string result;
	int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), nullptr, 0, nullptr, nullptr);
	if (len <= 0)
		return result;

	char* buffer = new char[len + 1];
	if (buffer == nullptr)
		return result;
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, nullptr, nullptr);
	buffer[len] = '\0';
	result.append(buffer);
	delete[] buffer;

	return result;
}

std::wstring CommonFunction::StringToWString(const std::string& str)
{
	std::wstring result;
	int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), nullptr, 0);
	if (len <= 0)
		return result;

	wchar_t* buffer = new wchar_t[len + 1];
	if (buffer == nullptr)
		return result;
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), buffer, len);
	buffer[len] = '\0';
	result.append(buffer);
	delete[] buffer;

	return result;
}

std::wstring CommonFunction::GetFileFolder(const std::wstring& filePath)
{
	std::wstring strPath = filePath;
	std::wstring::size_type position = strPath.find_last_of(L'\\');
	if (position != std::wstring::npos)
	{
		strPath.erase(position);
	}
	return strPath;
}

std::wstring CommonFunction::GetLocalTimeString()
{
	SYSTEMTIME st = { 0 };
	GetLocalTime(&st);

	wchar_t szTime[256];
	int ncopy = wnsprintfW(szTime, sizeof(szTime)/sizeof(szTime[0])-1, L"%d%02d%02d-%02d%02d%02d", st.wYear, st.wMonth, st.wDay,
		st.wHour, st.wMinute, st.wSecond);

	szTime[ncopy] = 0;

	return szTime;
}

SYSTEMTIME CommonFunction::LocalTimeStringToSYSTEMTIME(std::wstring str)
{
	SYSTEMTIME st = { 0 };
	int a = 0;
	swscanf(str.c_str(), L"%04d%02d%02d-%02d%02d%02d.txt", &st.wYear, &st.wMonth, &st.wDay, &st.wHour, &st.wMinute, &st.wSecond);
	return st;
}

double CommonFunction::IntervalOfSYSTEMTIME(SYSTEMTIME start, SYSTEMTIME end)
{
	ULARGE_INTEGER fTime1;/*FILETIME*/
	ULARGE_INTEGER fTime2;/*FILETIME*/


	SystemTimeToFileTime(&start, (FILETIME*)&fTime1);
	SystemTimeToFileTime(&end, (FILETIME*)&fTime2);
	ULONGLONG dft = fTime2.QuadPart > fTime1.QuadPart ? fTime2.QuadPart - fTime1.QuadPart : fTime1.QuadPart - fTime2.QuadPart;
	return (double)dft / double(10000000);
}


std::wstring CommonFunction::GetProgramDataFolder()
{
	wchar_t* pwszFolder = NULL;
	HRESULT hr = ::SHGetKnownFolderPath(FOLDERID_ProgramData, 0, NULL, &pwszFolder);
	if (SUCCEEDED(hr) && (NULL!=pwszFolder) )
	{
		std::wstring strFolder = pwszFolder;
		CoTaskMemFree(pwszFolder);


		//create sub folder
		strFolder += L"\\Nextlabs\\officeaddin";

		int nRes =SHCreateDirectoryExW(NULL, strFolder.c_str(), NULL);
		if ((nRes!=ERROR_SUCCESS) &&
			(nRes!= ERROR_ALREADY_EXISTS) &&
			(nRes!=ERROR_FILE_EXISTS) )
		{
			strFolder = L"";
		}


		return strFolder;
	}
	return L"";
}
