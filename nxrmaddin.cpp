#include "stdafx.h"
#include "nxrmaddin.h"
#include "nxrmext2.h"

const GUID CLSID_nxrmAddin = { 0xcca3189, 0xf325, 0x4d58,{ 0xab, 0x6d, 0x21, 0x2c, 0xd7, 0x6c, 0x33, 0x22 } };
extern HMODULE g_hModule;

LONG	g_unxrmaddinInstanceCount = 0;
LONG	g_unxrmext2InstanceCount = 0;

static HRESULT install_com_component(WCHAR *addin_path, const WCHAR* wszClsGuidString);
static HRESULT install_powerpoint_addin(void);
static HRESULT install_excel_addin(void);
static HRESULT install_word_addin(void);


static HRESULT uninstall_com_component(void);
static HRESULT uninstall_powerpoint_addin(void);
static HRESULT uninstall_excel_addin(void);
static HRESULT uninstall_word_addin(void);

Inxrmaddin::Inxrmaddin()
{
	m_uRefCount = 1;
	m_uLockCount = 0;
}


Inxrmaddin::~Inxrmaddin()
{
}


STDMETHODIMP Inxrmaddin::QueryInterface(REFIID riid, void **ppobj)
{
	HRESULT hRet = S_OK;

	IUnknown *punk = NULL;

	*ppobj = NULL;

	do
	{
		if ((IID_IUnknown == riid) || (IID_IClassFactory == riid))
		{
			punk = (IUnknown *)this;
		}
		else
		{
			hRet = E_NOINTERFACE;
			break;
		}

		AddRef();

		*ppobj = punk;

	} while (FALSE);

	return hRet;
}

STDMETHODIMP Inxrmaddin::CreateInstance(IUnknown * pUnkOuter, REFIID riid, void ** ppvObject)
{
	HRESULT hr = S_OK;

	nxrmExt2 *p = NULL;

	do
	{
		if (pUnkOuter)
		{
			*ppvObject = NULL;
			hr = CLASS_E_NOAGGREGATION;
			break;
		}

		p = new nxrmExt2;

		if (!p)
		{
			*ppvObject = NULL;
			hr = E_OUTOFMEMORY;
			break;
		}

		InterlockedIncrement(&g_unxrmext2InstanceCount);

		hr = p->QueryInterface(riid, ppvObject);

		p->Release();

	} while (FALSE);

	return hr;
}

STDMETHODIMP Inxrmaddin::LockServer(BOOL fLock)
{
	if (fLock)
	{
		m_uLockCount++;
	}
	else
	{
		if (m_uLockCount > 0)
			m_uLockCount--;
	}

	return m_uLockCount;
}

STDMETHODIMP_(ULONG) Inxrmaddin::AddRef()
{
	m_uRefCount++;

	return m_uRefCount;
}

STDMETHODIMP_(ULONG) Inxrmaddin::Release()
{
	ULONG uCount = 0;

	if (m_uRefCount)
		m_uRefCount--;

	uCount = m_uRefCount;

	if (!uCount && (m_uLockCount == 0))
	{
		delete this;
		InterlockedDecrement(&g_unxrmaddinInstanceCount);
	}

	return uCount;
}

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	HRESULT  hr = E_OUTOFMEMORY;

	Inxrmaddin *InxrmInstance = NULL;

	if (IsEqualCLSID(rclsid, CLSID_nxrmAddin))
	{
		InxrmInstance = new Inxrmaddin;

		if (InxrmInstance)
		{
			InterlockedIncrement(&g_unxrmaddinInstanceCount);
			hr = InxrmInstance->QueryInterface(riid, ppv);
			InxrmInstance->Release();
		}
	}
	else
	{
		hr = CLASS_E_CLASSNOTAVAILABLE;
	}

	return(hr);
}

STDAPI DllCanUnloadNow(void)
{
	if (g_unxrmaddinInstanceCount == 0 && g_unxrmext2InstanceCount == 0)
	{
		return S_OK;
	}
	else
	{
		return S_FALSE;
	}
}

STDAPI DllUnregisterServer(void)
{
	uninstall_powerpoint_addin();

	uninstall_excel_addin();

	uninstall_word_addin();

	uninstall_com_component();

	return S_OK;
}

STDAPI DllRegisterServer(void)
{
	HRESULT nRet = S_OK;

	WCHAR module_path[MAX_PATH] = { 0 };
	OLECHAR* ClsGuidString = NULL;

	do
	{
		if (!GetModuleFileNameW(g_hModule, module_path, sizeof(module_path) / sizeof(WCHAR)))
		{
			nRet = E_UNEXPECTED;
			break;
		}

		//create guid string	
		StringFromCLSID(CLSID_nxrmAddin, &ClsGuidString);


		nRet = install_com_component(module_path, ClsGuidString);
		if (S_OK != nRet)
		{
			break;
		}

		nRet = install_powerpoint_addin();
		if (S_OK != nRet)
		{
			break;
		}

		nRet = install_excel_addin();
		if (S_OK != nRet)
		{
			break;
		}

		nRet = install_word_addin();
		if (S_OK != nRet)
		{
			break;
		}

	} while (FALSE);

	if (ClsGuidString!=NULL)
	{
		::CoTaskMemFree(ClsGuidString);
		ClsGuidString = NULL;
	}

	if (nRet != S_OK)
	{
		DllUnregisterServer();
	}

	return nRet;
}


static HRESULT create_key_with_default_value(
	const HKEY	root,
	const WCHAR *parent,
	const WCHAR *key,
	const WCHAR *default_value)
{
	HRESULT nRet = S_OK;

	HKEY hParent = NULL;
	HKEY hKey = NULL;

	do
	{
		if (ERROR_SUCCESS != RegOpenKeyExW(root,
			parent,
			0,
			KEY_WRITE,
			&hParent))
		{
			nRet = E_UNEXPECTED;
			break;
		}

		if (ERROR_SUCCESS != RegCreateKey(hParent,
			key,
			&hKey))
		{
			nRet = E_UNEXPECTED;
			break;
		}

		if (!default_value)
		{
			break;
		}

		if (ERROR_SUCCESS != RegSetValueExW(hKey,
			NULL,
			0,
			REG_SZ,
			(const BYTE*)default_value,
			(DWORD)(wcslen(default_value) + 1) * sizeof(WCHAR)))
		{
			nRet = E_UNEXPECTED;
			break;
		}

	} while (FALSE);

	if (hKey)
	{
		RegCloseKey(hKey);
		hKey = NULL;
	}

	if (hParent)
	{
		RegCloseKey(hParent);
		hParent = NULL;
	}

	return nRet;
}

static HRESULT set_value_content(
	const WCHAR *key,
	const WCHAR *valuename,
	const WCHAR *content)
{
	HRESULT nRet = S_OK;

	HKEY hKey = NULL;

	do
	{
		if (ERROR_SUCCESS != RegOpenKeyExW(HKEY_CLASSES_ROOT,
			key,
			0,
			KEY_SET_VALUE,
			&hKey))
		{
			nRet = E_UNEXPECTED;
			break;
		}

		if (ERROR_SUCCESS != RegSetValueExW(hKey,
			valuename,
			0,
			REG_SZ,
			(const BYTE*)content,
			(DWORD)(wcslen(content) + 1) * sizeof(WCHAR)))
		{
			nRet = E_UNEXPECTED;
			break;
		}

	} while (FALSE);

	if (hKey)
	{
		RegCloseKey(hKey);
		hKey = NULL;
	}

	return nRet;
}

static HRESULT create_dword_value(
	const HKEY	root,
	const WCHAR	*key,
	const WCHAR	*value_name,
	const DWORD	value)
{
	HRESULT hr = S_OK;

	HKEY hKey = NULL;

	do
	{
		//
		// the key must has been created in advance
		//
		if (ERROR_SUCCESS != RegOpenKeyExW(root,
			key,
			0,
			KEY_WRITE,
			&hKey))
		{
			hr = E_UNEXPECTED;
			break;
		}

		if (ERROR_SUCCESS != RegSetValueExW(hKey,
			value_name,
			0,
			REG_DWORD,
			(const BYTE*)&value,
			sizeof(value)))
		{
			hr = E_UNEXPECTED;
			break;
		}

	} while (FALSE);

	if (hKey)
	{
		RegCloseKey(hKey);
		hKey = NULL;
	}

	return hr;
}

static HRESULT create_sz_value(
	const HKEY	root,
	const WCHAR	*key,
	const WCHAR	*value_name,
	const WCHAR	*value)
{
	HRESULT hr = S_OK;

	HKEY hKey = NULL;

	do
	{
		//
		// the key must has been created in advance
		//
		if (ERROR_SUCCESS != RegOpenKeyExW(root,
			key,
			0,
			KEY_WRITE,
			&hKey))
		{
			hr = E_UNEXPECTED;
			break;
		}

		if (ERROR_SUCCESS != RegSetValueExW(hKey,
			value_name,
			0,
			REG_SZ,
			(const BYTE*)value,
			(DWORD)(wcslen(value) + 1) * sizeof(WCHAR)))
		{
			hr = E_UNEXPECTED;
			break;
		}

	} while (FALSE);

	if (hKey)
	{
		RegCloseKey(hKey);
		hKey = NULL;
	}

	return hr;

}

static HRESULT delete_key(const HKEY root, const WCHAR *parent, const WCHAR *key)
{
	HRESULT nRet = S_OK;

	HKEY hKey = NULL;

	do
	{
		if (ERROR_SUCCESS != RegOpenKeyExW(root,
			parent,
			0,
			DELETE | KEY_SET_VALUE,
			&hKey))
		{
			nRet = E_UNEXPECTED;
			break;
		}

		if (ERROR_SUCCESS != RegDeleteTreeW(hKey, key))
		{
			nRet = E_UNEXPECTED;
			break;
		}

	} while (FALSE);

	if (hKey)
	{
		RegCloseKey(hKey);
		hKey = NULL;
	}

	return nRet;
}


static HRESULT install_com_component(WCHAR *addin_path, const WCHAR* wszClsGuid)
{
	HRESULT hr = S_OK;

	do
	{
		//
		// install component under CLSID 
		//
		std::wstring wstrClsIDKey = L"CLSID";
		hr = create_key_with_default_value(HKEY_CLASSES_ROOT,
			wstrClsIDKey.c_str(),
			wszClsGuid,
			NXRMADDIN_NAME);

		if (S_OK != hr)
		{
			break;
		}

		std::wstring wstrInstallClsIDKey = wstrClsIDKey + L"\\" + wszClsGuid;
		hr = create_key_with_default_value(HKEY_CLASSES_ROOT,
			wstrInstallClsIDKey.c_str(),
			L"InprocServer32",
			addin_path);

		if (S_OK != hr)
		{
			break;
		}

		std::wstring wstrInstallInproc32Key = wstrInstallClsIDKey + L"\\InprocServer32";
		hr = set_value_content(wstrInstallInproc32Key.c_str(),
			L"ThreadingModel",
			L"Apartment");

		if (S_OK != hr)
		{
			break;
		}

		//
		// install component under HKEY_CLASSES_ROOT
		//
		hr = create_key_with_default_value(HKEY_CLASSES_ROOT,
			NULL,
			NXRMADDIN_NAME,
			L"nxlrmaddin Class");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_key_with_default_value(HKEY_CLASSES_ROOT,
			NXRMADDIN_NAME,
			L"CLSID",
			wszClsGuid);

		if (S_OK != hr)
		{
			break;
		}

	} while (FALSE);

	return hr;
}

static HRESULT install_powerpoint_addin(void)
{
	HRESULT hr = S_OK;

	WCHAR addin_key[260] = { 0 };

	do
	{
		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_INSTALL_POWERPOINT_ADDIN_KEY);

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			L"\\");

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_NAME);

		//
		// create powerpoint addin first in case office is not installed
		//
		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NULL,
			NXRMADDIN_INSTALL_POWERPOINT_ADDIN_KEY,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_POWERPOINT_ADDIN_KEY,
			NXRMADDIN_NAME,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_DESCRIPTION_VALUE,
			L"Enable NextLabs Rights Management service for PowerPoint");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_FRIENDLYNAME_VALUE,
			L"NextLabs Rights Management for PowerPoint");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_dword_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_LOADBEHAVIOR_VALUE,
			3);

		if (S_OK != hr)
		{
			break;
		}

	} while (FALSE);

	return hr;
}

static HRESULT install_excel_addin(void)
{
	HRESULT hr = S_OK;

	WCHAR addin_key[260] = { 0 };

	do
	{
		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_INSTALL_EXCEL_ADDIN_KEY);

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			L"\\");

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_NAME);

		//
		// create excel addin first in case office is not installed
		//
		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NULL,
			NXRMADDIN_INSTALL_EXCEL_ADDIN_KEY,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_EXCEL_ADDIN_KEY,
			NXRMADDIN_NAME,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_DESCRIPTION_VALUE,
			L"Enable NextLabs Rights Management service for Excel");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_FRIENDLYNAME_VALUE,
			L"NextLabs Rights Management for Excel");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_dword_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_LOADBEHAVIOR_VALUE,
			3);

		if (S_OK != hr)
		{
			break;
		}

	} while (FALSE);

	return hr;

}
static HRESULT install_word_addin(void)
{
	HRESULT hr = S_OK;

	WCHAR addin_key[260] = { 0 };

	do
	{
		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_INSTALL_WINWORD_ADDIN_KEY);

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			L"\\");

		wcscat_s((WCHAR*)addin_key,
			sizeof(addin_key) / sizeof(WCHAR),
			NXRMADDIN_NAME);

		//
		// create word addin first in case office is not installed
		//
		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NULL,
			NXRMADDIN_INSTALL_WINWORD_ADDIN_KEY,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_key_with_default_value(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_WINWORD_ADDIN_KEY,
			NXRMADDIN_NAME,
			NULL);

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_DESCRIPTION_VALUE,
			L"Enable NextLabs Rights Management service for Word");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_sz_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_FRIENDLYNAME_VALUE,
			L"NextLabs Rights Management for Word");

		if (S_OK != hr)
		{
			break;
		}

		hr = create_dword_value(HKEY_LOCAL_MACHINE,
			(const WCHAR*)addin_key,
			NXRMADDIN_INSTALL_LOADBEHAVIOR_VALUE,
			3);

		if (S_OK != hr)
		{
			break;
		}

	} while (FALSE);

	return hr;

}

static HRESULT uninstall_com_component(void)
{
	HRESULT hr = S_OK;

	do
	{
		OLECHAR* ClsGuidString = NULL;
		StringFromCLSID(CLSID_nxrmAddin, &ClsGuidString);

		hr = delete_key(HKEY_CLASSES_ROOT,
			L"CLSID",
			ClsGuidString);

		hr = delete_key(HKEY_CLASSES_ROOT,
			NULL,
			NXRMADDIN_NAME);

	} while (FALSE);

	return hr;
}

static HRESULT uninstall_powerpoint_addin(void)
{
	HRESULT hr = S_OK;

	do
	{
		hr = delete_key(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_POWERPOINT_ADDIN_KEY,
			NXRMADDIN_NAME);

	} while (FALSE);

	return hr;
}

static HRESULT uninstall_excel_addin(void)
{
	HRESULT hr = S_OK;

	do
	{
		hr = delete_key(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_EXCEL_ADDIN_KEY,
			NXRMADDIN_NAME);

	} while (FALSE);

	return hr;

}

static HRESULT uninstall_word_addin(void)
{
	HRESULT hr = S_OK;

	do
	{
		hr = delete_key(HKEY_LOCAL_MACHINE,
			NXRMADDIN_INSTALL_WINWORD_ADDIN_KEY,
			NXRMADDIN_NAME);

	} while (FALSE);

	return hr;

}
