#ifndef AZURE_SQL_LOG_H
#define AZURE_SQL_LOG_H

#include <windows.h>
#include <list>

#define MAX_LOG_MESSAGE_SIZE_CHARS 1024

class CLogContent
{
public:
	CLogContent() : m_wszLog{ 0 } {}
	static void Init();

public:
	void *operator new(size_t size);
	void operator delete(void*);


public:
	wchar_t m_wszLog[MAX_LOG_MESSAGE_SIZE_CHARS];
	int m_nlogLevel;

private:
	// private heap used for Alloc memory for object of this class.
	static HANDLE m_priHeap;
};

class CLog
{
public:
	~CLog();
	CLog();
	bool Init(int nLogLevel, DWORD logPolicy);
	int WriteLog(int lvl,
		const char* file,
		int line,
		const wchar_t* fmt,
		...);


protected:
	std::wstring GetLogFile();
	std::wstring GetLogFileFolder();
	void CleanLogFile(DWORD intervalSec);

#if ASYN_LOG
private:
	static DWORD WINAPI WriteLogThread(LPVOID pParam);

private:
	CRITICAL_SECTION  m_csLogs;
	std::list<CLogContent*> m_lstLogs;
#endif

	HANDLE   m_hlogFile;

};
extern CLog theLog;
#endif 