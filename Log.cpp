#define ASYN_LOG   0
#include "stdafx.h"
#include "Log.h"
#include "CommonFunction.h"
#include <atlbase.h>

CLog theLog;

HANDLE CLogContent::m_priHeap = NULL;

void CLogContent::Init()
{
	if (m_priHeap==NULL)
	{
		m_priHeap = HeapCreate(0, 0, 0);
	}
}

void* CLogContent::operator new(size_t size)
{
	if (m_priHeap!=NULL)
	{
		return HeapAlloc(m_priHeap, 0, size);
	}
	return NULL;
}

void CLogContent::operator delete(void* p)
{
	if (m_priHeap!=NULL)
	{
		HeapFree(m_priHeap, 0, p);
	}
}

CLog::CLog()
{
	m_hlogFile = NULL;
}

CLog::~CLog()
{
	if (m_hlogFile!=NULL && m_hlogFile!=INVALID_HANDLE_VALUE)
	{
		FlushFileBuffers(m_hlogFile);
		CloseHandle(m_hlogFile);
		m_hlogFile = NULL;
	}
}

bool CLog::Init(int nLogLevel, DWORD logPolicy)
{
	CleanLogFile(7 * 24 * 3600);

	//init logcontent
	CLogContent::Init();

	//Create log file
	std::wstring wstrLogFileName = GetLogFile();
	m_hlogFile = ::CreateFileW(wstrLogFileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, 0, NULL);

	

#if ASYN_LOG
	InitializeCriticalSection(&m_csLogs);
	CloseHandle(CreateThread(NULL, 0, WriteLogThread, this, 0, NULL));
#endif
	
	return true;
}

int CLog::WriteLog(int lvl, const char* file, int line, const wchar_t* fmt, ...)
{
	int nLog=0;

#if ASYN_LOG
	CLogContent* pLogContent = new CLogContent();

	//format log content
	va_list args;
	va_start(args, fmt);
	nLog = _vsnwprintf(pLogContent->m_wszLog, _countof(pLogContent->m_wszLog)-1, fmt, args);
	va_end(args);

	//pLogContent->m_wszLog[nLog] = 0;	// vswprintf return -1 when _Format size exceed _BufferCount
	pLogContent->m_nlogLevel = lvl;

	{
		CriticalSectionLock lockLogs(&m_csLogs);
		m_lstLogs.push_back(pLogContent);
	}
#else
	CLogContent* pLogContent = new CLogContent();

	//format log content
	va_list args;
	va_start(args, fmt);
	nLog = _vsnwprintf(pLogContent->m_wszLog, _countof(pLogContent->m_wszLog) - 1, fmt, args);
	va_end(args);

	if (m_hlogFile!=NULL && m_hlogFile!=INVALID_HANDLE_VALUE)
	{
		DWORD dwBytesWriteen = 0;
		WriteFile(m_hlogFile, pLogContent->m_wszLog, wcslen(pLogContent->m_wszLog) * sizeof(wchar_t), &dwBytesWriteen, NULL);
	}
#endif

	return nLog;
}

std::wstring CLog::GetLogFile()
{
	std::wstring strAppDataFolder = CommonFunction::GetProgramDataFolder();

	::OutputDebugStringW(strAppDataFolder.c_str());
	if (!strAppDataFolder.empty())
	{
		std::wstring strLogFile = strAppDataFolder + L"\\log";

		CreateDirectoryW(strLogFile.c_str(), NULL);
		strLogFile += L"\\";
		strLogFile += CommonFunction::GetLocalTimeString();
		strLogFile += L".txt";
		return strLogFile;
	}

	return L"";
}

std::wstring CLog::GetLogFileFolder()
{
	std::wstring strAppDataFolder = CommonFunction::GetProgramDataFolder();
	::OutputDebugStringW(strAppDataFolder.c_str());
	if (!strAppDataFolder.empty())
	{
		std::wstring strLogFile = strAppDataFolder + L"\\log";

		CreateDirectoryW(strLogFile.c_str(), NULL);
		strLogFile += L"\\";
		return strLogFile;
	}
	return L"";
}

void CLog::CleanLogFile(DWORD intervalSec)
{
	std::wstring folder = GetLogFileFolder();
	std::wstring file = folder + L"\\*.txt";
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = FindFirstFile(file.c_str(), &FindFileData);
	
	SYSTEMTIME end = { 0 };
	GetLocalTime(&end);

	if (INVALID_HANDLE_VALUE == hFind)
		return;
	do
	{
		if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
		}
		else
		{
			SYSTEMTIME start = CommonFunction::LocalTimeStringToSYSTEMTIME(FindFileData.cFileName);
			double inter = CommonFunction::IntervalOfSYSTEMTIME(start, end);
			if (inter > intervalSec)
			{
				std::string cur = CommonFunction::WStringToString(folder + FindFileData.cFileName);
				remove(cur.c_str());
			}
		}
	} while (FindNextFile(hFind, &FindFileData));

	FindClose(hFind);
}

#if ASYN_LOG
DWORD WINAPI CLog::WriteLogThread(LPVOID pParam)
{
	CLog* thisLog = reinterpret_cast<CLog*>(pParam);

	while (true)
	{
		Sleep(10 * 1000);//the log don't need write real time, so we don't use WaitSingleEvent, just sleep

		//get logs
		std::list<CLogContent*> lstLogs;
		//copy list to limit the lock time
		{
			CriticalSectionLock lockLogs(&thisLog->m_csLogs);
			lstLogs.assign(thisLog->m_lstLogs.begin(), thisLog->m_lstLogs.end());
			thisLog->m_lstLogs.clear();
		}

		//write log
		for (std::list<CLogContent*>::iterator itLog = lstLogs.begin(); itLog != lstLogs.end(); itLog++)
		{
			CLogContent* pLog = *itLog;
			CELogS::Instance()->Log(pLog->m_nlogLevel, pLog->m_wszLog);
			delete pLog;
		}
	}


}
#endif 
