#pragma once
#include <Unknwn.h>
#include <string>

class IEventBase
{
public:
	virtual STDMETHODIMP GetActiveDoc(
		/*[in,out]*/ std::wstring &ActiveDoc) = 0;

	virtual STDMETHODIMP GetActiveRights(
		/*[in,out]*/ ULONGLONG &ActiveRights) = 0;

	virtual HRESULT RefreshActiveRights(void) = 0;

	virtual void Init(IDispatch *pRibbonUI, BSTR ActiveDoc, ULONGLONG &ActiveRights) = 0;
};