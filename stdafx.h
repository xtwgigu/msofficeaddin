// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <Ole2.h>
#include <OCIdl.h>
#include <stdexcpt.h>
#include <wchar.h>

#include <string>
#include <vector>
#include <map>
//#import "C:\Program Files (x86)\Microsoft Office\root\VFS\ProgramFilesCommonX86\Microsoft Shared\OFFICE16\MSO.DLL" rename("RGB","MsRGB") raw_interfaces_only rename_namespace("Office2016")
//#import "C:\Program Files (x86)\Microsoft Office\root\Office16\EXCEL.EXE" rename("RGB","MsRGB") rename("DialogBox","MsDialogBox") rename("CopyFile","MsCopyFile") raw_interfaces_only rename_namespace("Excel2016")
//#import "C:\Program Files (x86)\Microsoft Office\root\Office16\MSWORD.OLB" rename("ExitWindows","MsExitWindows") raw_interfaces_only rename_namespace("Word2016")
//#import "C:\Program Files (x86)\Microsoft Office\root\Office16\MSPPT.OLB" rename("RGB","MsRGB") raw_interfaces_only rename_namespace("PowerPoint2016")
//#import "C:\Program Files (x86)\Common Files\DESIGNER\MSADDNDR.OLB"  raw_interfaces_only