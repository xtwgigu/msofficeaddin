#ifndef COMMON_FUNCTION_H
#define COMMON_FUNCTION_H

#include <string>
#include <atlbase.h>
class CommonFunction
{
public:
	CommonFunction();
	~CommonFunction();

	static std::string WStringToString(const std::wstring& wstr);
	static std::wstring StringToWString(const std::string& str);
	static std::wstring GetFileFolder(const std::wstring& filePath);
	static std::wstring GetProgramDataFolder();
	static std::wstring GetLocalTimeString();
	static SYSTEMTIME LocalTimeStringToSYSTEMTIME(std::wstring);
	static double IntervalOfSYSTEMTIME(SYSTEMTIME start, SYSTEMTIME end);

	static std::wstring FormatConnectStr(const TCHAR* pszDSN, const TCHAR* username, const TCHAR* pwd, const TCHAR* db, const TCHAR* auth);
};

#endif // !COMMON_FUNCTION_H

