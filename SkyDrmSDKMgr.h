#pragma once
#include <windows.h>
#include "SDWL/SDLAPI.h"

typedef std::vector<std::pair<SDRmFileRight, std::vector<SDR_WATERMARK_INFO>>>  VECTOR_RIGHTS;

class SkyDrmSDKMgr
{
public:
	static SkyDrmSDKMgr* Instance()
	{
		static SkyDrmSDKMgr* pInstance = new SkyDrmSDKMgr();
		return pInstance;
	}

private:
	SkyDrmSDKMgr();
	SkyDrmSDKMgr(const SkyDrmSDKMgr&) {}
	~SkyDrmSDKMgr();

public:
	bool GetCurrentLoggedInUser();
	HRESULT CheckRights(const WCHAR* wszFullFileName, ULONGLONG& RightMask, ULONGLONG& CustomRightsMask);
	bool EditSaveFile(const std::wstring &filepath, const std::wstring& originalNXLfilePath = L"", bool deletesource = false, bool exitedit = false);
	bool EditCopyFile(const std::wstring &filepath, std::wstring& destpath);
protected:
	std::string GetRMXPasscode(){ return m_strRMXPasscode; }
	bool GetNxlFileRightsByRMUser(const std::wstring& nxlfilepath, VECTOR_RIGHTS &rightsAndWatermarks);
	bool GetNxlFileRightsByRMInstance(const std::wstring& nxlfilepath, VECTOR_RIGHTS &rightsAndWatermarks);
	bool HaveRight(const VECTOR_RIGHTS& vecRightAndWaterMark, SDRmFileRight right);
	DWORD SumRights(const VECTOR_RIGHTS& vecRights);

private:
	ISDRmcInstance* m_pRmcInstance;
	ISDRmTenant *m_pRmTenant;
	ISDRmUser *m_pRmUser;
	std::string m_strRMXPasscode;
};

