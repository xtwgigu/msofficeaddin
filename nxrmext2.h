#pragma once
#include "import/msaddndr.tlh"
#include "import/mso2016.tlh"
#include "ribbonrights.h"
#include "IEventBase.h"

typedef enum _nxrmOfficeAppType
{
	OfficeAppInvalid = 0,
	OfficeAppPowerpoint = 0x7001,
	OfficeAppWinword,
	OfficeAppExcel,
	OfficeAppOutlook
}nxrmOfficeAppType;

#define NXRMCOREUI_ONLOAD_PROC_NAME					L"OnLoad"
#define NXRMCOREUI_CHECKMSOBUTTONSTATUS_PROC_NAME	L"CheckMsoButtonStatus"


#define NXRMCOREUI_CHECKMSOBUTTONSTATUS_ID	(0x5001)
#define NXRMCOREUI_ONLOAD_ID				(0x8001)

class nxrmExt2 : public AddInDesignerObjects::_IDTExtensibility2, public Office2016::IRibbonExtensibility
{
public:
	nxrmExt2();
	~nxrmExt2();

	//interface for IUnknow
	HRESULT STDMETHODCALLTYPE QueryInterface(
		/* [in] */ REFIID riid,
		/* [annotation][iid_is][out] */
		_COM_Outptr_  void **ppvObject);

	ULONG STDMETHODCALLTYPE AddRef(void);

	ULONG STDMETHODCALLTYPE Release(void);

	//interface for IDispatch
	HRESULT STDMETHODCALLTYPE GetTypeInfoCount(
		/* [out] */ __RPC__out UINT *pctinfo);

	HRESULT STDMETHODCALLTYPE GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ __RPC__deref_out_opt ITypeInfo **ppTInfo);

	HRESULT STDMETHODCALLTYPE GetIDsOfNames(
		/* [in] */ __RPC__in REFIID riid,
		/* [size_is][in] */ __RPC__in_ecount_full(cNames) LPOLESTR *rgszNames,
		/* [range][in] */ __RPC__in_range(0, 16384) UINT cNames,
		/* [in] */ LCID lcid,
		/* [size_is][out] */ __RPC__out_ecount_full(cNames) DISPID *rgDispId);

	HRESULT STDMETHODCALLTYPE Invoke(
		/* [annotation][in] */
		_In_  DISPID dispIdMember,
		/* [annotation][in] */
		_In_  REFIID riid,
		/* [annotation][in] */
		_In_  LCID lcid,
		/* [annotation][in] */
		_In_  WORD wFlags,
		/* [annotation][out][in] */
		_In_  DISPPARAMS *pDispParams,
		/* [annotation][out] */
		_Out_opt_  VARIANT *pVarResult,
		/* [annotation][out] */
		_Out_opt_  EXCEPINFO *pExcepInfo,
		/* [annotation][out] */
		_Out_opt_  UINT *puArgErr);

	//interface for _IDTExtensibility2
	HRESULT STDMETHODCALLTYPE OnConnection(
		/*[in]*/ IDispatch * Application,
		/*[in]*/ AddInDesignerObjects::ext_ConnectMode ConnectMode,
		/*[in]*/ IDispatch * AddInInst,
		/*[in]*/ SAFEARRAY * * custom);

	HRESULT STDMETHODCALLTYPE OnDisconnection(
		/*[in]*/ AddInDesignerObjects::ext_DisconnectMode RemoveMode,
		/*[in]*/ SAFEARRAY * * custom);

	HRESULT STDMETHODCALLTYPE OnAddInsUpdate(
		/*[in]*/ SAFEARRAY * * custom);

	HRESULT STDMETHODCALLTYPE OnStartupComplete(
		/*[in]*/ SAFEARRAY * * custom);

	HRESULT STDMETHODCALLTYPE OnBeginShutdown(
		/*[in]*/ SAFEARRAY * * custom);


	//interface for 
	HRESULT STDMETHODCALLTYPE GetCustomUI(
		/*[in]*/ BSTR RibbonID,
		/*[out,retval]*/ BSTR * RibbonXml);


protected:
	HRESULT STDMETHODCALLTYPE OnCheckMsoButtonStatus(/*[in]*/ Office2016::IRibbonControl *pControl,
		                                         /*[out, retval]*/ VARIANT_BOOL *pvarfEnabled);
	 
	HRESULT STDMETHODCALLTYPE OnLoad(/*[in]*/ IDispatch	*RibbonUI);



protected:
	HRESULT Attache2Word(void);
	HRESULT Attache2Powerpoint(void);
	HRESULT Attache2Excel(void);

	HRESULT InitWordEventSink(void);
	HRESULT InitPowerpointEventSink(void);
	HRESULT InitExcelEventSink(void);

private:
	ULONG				m_uRefCount;

	IDispatch			*m_pAppObj;
	IDispatch			*m_pRibbonUI;
	nxrmOfficeAppType	m_OfficeAppType;

	IEventBase			*m_pOfficeEventSink;

	struct _case_insensitive_cmp
	{
		bool operator()(const std::wstring &str1, const std::wstring &str2) const
		{
			return _wcsicmp(str1.c_str(), str2.c_str()) < 0;
		}
	};
	std::map<std::wstring, RIBBON_ID_INFO, _case_insensitive_cmp>	m_RibbonRightsMap;

};

