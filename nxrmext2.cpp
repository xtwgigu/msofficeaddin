#include "stdafx.h"
#include "nxrmext2.h"
#include "ribbonrights.h"
#include "rightsdef.h"
#include "officelayout.h"
#include "wordevents.h"
#include "powerpointevents.h"
#include "excelevents.h"
#include "SkyDrmSDKMgr.h"
#include "import/excel2016.tlh"
#include "import/msword2016.tlh"
#include "import/msppt2016.tlh"
#include "HookManager.h"

extern LONG g_unxrmext2InstanceCount;

extern "C" const std::vector<RIBBON_ID_INFO> g_powerpoint_16_ribbon_info = { \
{L"TabInfo", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabOfficeStart", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabRecent", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileClose", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSave", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FilePrintQuick", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabSave", BUILTIN_RIGHT_SAVEAS | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabPrint", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabShare", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabPublish", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ApplicationOptionsDialog", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"AdvancedFileProperties", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"UpgradeDocument", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSendAsAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsPdfEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsXpsEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileInternetFax", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabHome", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabInsert", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabDesign", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabTransitions", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabAnimations", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabSlideShow", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabReview", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabDeveloper", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabView", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenshotInsertGallery", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenClipping", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"OleObjectctInsert", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"OleObjectInsertMenu", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Paste", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Cut", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Copy", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL} \
};

extern "C" const std::vector<RIBBON_ID_INFO> g_excel_16_ribbon_info = { \
{L"TabInfo", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabOfficeStart", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabRecent", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileClose", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSave", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FilePrintQuick", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabSave", BUILTIN_RIGHT_SAVEAS | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabPrint", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabShare", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabPublish", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Publish2Tab", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ApplicationOptionsDialog", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"AdvancedFileProperties", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"UpgradeDocument", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSendAsAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsPdfEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsXpsEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileInternetFax", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabHome", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabInsert", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabFormulas", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabReview", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabData", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"SheetMoveOrCopy", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenshotInsertGallery", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenClipping", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"OleObjectctInsert", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Paste", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Cut", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Copy", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"CopyAsPicture", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL} \
};

extern "C" const std::vector<RIBBON_ID_INFO> g_word_16_ribbon_info = { \
{L"TabInfo", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabOfficeStart", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabRecent", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileClose", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSave", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FilePrintQuick", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabSave", BUILTIN_RIGHT_SAVEAS | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabPrint", BUILTIN_RIGHT_PRINT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabShare", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabPublish", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ApplicationOptionsDialog", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"UpgradeDocument", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"FileSendAsAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsPdfEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileEmailAsXpsEmailAttachment", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"FileInternetFax", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabHome", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabInsert", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabWordDesign", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabPageLayoutWord", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabReferences", BUILTIN_RIGHT_EDIT | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"TabMailings", BUILTIN_RIGHT_SEND, 0ULL}, \
{L"TabReviewWord", BUILTIN_RIGHT_VIEW | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenshotInsertGallery", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"ScreenClipping", BUILTIN_RIGHT_SCREENCAP | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"OleObjectInsertMenu", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"OleObjectctInsert", BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Paste", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Cut", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL}, \
{L"Copy", BUILTIN_RIGHT_CLIPBOARD | BUILTIN_RIGHT_DECRYPT, 0ULL} \
};


nxrmExt2::nxrmExt2()
{
	m_uRefCount = 1;

	m_pAppObj = NULL;
	m_pRibbonUI = NULL;
	m_OfficeAppType = OfficeAppInvalid;
	m_pOfficeEventSink = NULL;
}


nxrmExt2::~nxrmExt2()
{
}


HRESULT STDMETHODCALLTYPE nxrmExt2::QueryInterface(
	/* [in] */ REFIID riid,
	/* [annotation][iid_is][out] */
	_COM_Outptr_  void **ppvObject)
{
	HRESULT hRet = S_OK;

	void *punk = NULL;

	*ppvObject = NULL;

	do
	{
		if (IID_IUnknown == riid || IID_IDispatch == riid)
		{
			punk = this;
		}
		else if (__uuidof(AddInDesignerObjects::_IDTExtensibility2) == riid)
		{
			punk = dynamic_cast<AddInDesignerObjects::_IDTExtensibility2*>(this);
		}
		else if (__uuidof(Office2016::IRibbonExtensibility) == riid)
		{
			punk = dynamic_cast<Office2016::IRibbonExtensibility*>(this);
		}
		else
		{
			hRet = E_NOINTERFACE;
			break;
		}

		AddRef();
		*ppvObject = punk;

	} while (FALSE);

	return hRet;
}


ULONG STDMETHODCALLTYPE nxrmExt2::AddRef(void)
{
	m_uRefCount++;

	return m_uRefCount;
}

ULONG STDMETHODCALLTYPE nxrmExt2::Release(void)
{
	ULONG uCount = 0;

	if (m_uRefCount)
		m_uRefCount--;

	uCount = m_uRefCount;

	if (!uCount)
	{
		delete this;
		InterlockedDecrement(&g_unxrmext2InstanceCount);
	}

	return uCount;
}


HRESULT STDMETHODCALLTYPE nxrmExt2::GetTypeInfoCount(
	/* [out] */ __RPC__out UINT *pctinfo)
{
	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::GetTypeInfo(
	/* [in] */ UINT iTInfo,
	/* [in] */ LCID lcid,
	/* [out] */ __RPC__deref_out_opt ITypeInfo **ppTInfo)
{
	return E_NOTIMPL;
}


HRESULT STDMETHODCALLTYPE nxrmExt2::GetIDsOfNames(
	/* [in] */ __RPC__in REFIID riid,
	/* [size_is][in] */ __RPC__in_ecount_full(cNames) LPOLESTR *rgszNames,
	/* [range][in] */ __RPC__in_range(0, 16384) UINT cNames,
	/* [in] */ LCID lcid,
	/* [size_is][out] */ __RPC__out_ecount_full(cNames) DISPID *rgDispId)
{
	HRESULT hr = DISP_E_UNKNOWNNAME;

	UINT i = 0;

	for (i = 0; i < cNames; i++)
	{	
		if (wcscmp(rgszNames[i], NXRMCOREUI_CHECKMSOBUTTONSTATUS_PROC_NAME) == 0)
		{
			rgDispId[i] = NXRMCOREUI_CHECKMSOBUTTONSTATUS_ID;
			hr = S_OK;
		}
		else if (wcscmp(rgszNames[i], NXRMCOREUI_ONLOAD_PROC_NAME) == 0)
		{
			rgDispId[i] = NXRMCOREUI_ONLOAD_ID;
			hr = S_OK;
		}
		else
		{
			rgDispId[i] = DISPID_UNKNOWN;
		}
		
	}

	return hr;
}


HRESULT STDMETHODCALLTYPE nxrmExt2::Invoke(
	/* [annotation][in] */
	_In_  DISPID dispIdMember,
	/* [annotation][in] */
	_In_  REFIID riid,
	/* [annotation][in] */
	_In_  LCID lcid,
	/* [annotation][in] */
	_In_  WORD wFlags,
	/* [annotation][out][in] */
	_In_  DISPPARAMS *pDispParams,
	/* [annotation][out] */
	_Out_opt_  VARIANT *pVarResult,
	/* [annotation][out] */
	_Out_opt_  EXCEPINFO *pExcepInfo,
	/* [annotation][out] */
	_Out_opt_  UINT *puArgErr)
{
	HRESULT hr = DISP_E_MEMBERNOTFOUND;

	IDispatch *pPicDisp = NULL;
	VARIANT_BOOL bEnable = VARIANT_FALSE;
	BSTR Lable = NULL;

	do
	{
		switch (dispIdMember)
		{

		case NXRMCOREUI_CHECKMSOBUTTONSTATUS_ID:
			hr = OnCheckMsoButtonStatus((Office2016::IRibbonControl*)(pDispParams->rgvarg[0].pdispVal), &bEnable);
			if (SUCCEEDED(hr))
			{
				pVarResult->vt = VT_BOOL;
				pVarResult->boolVal = bEnable;
			}
			break;

		case NXRMCOREUI_ONLOAD_ID:
			hr = OnLoad((Office2016::IRibbonUI*)pDispParams->rgvarg[0].pdispVal);
			break;


		default:
			break;

		}

	} while (FALSE);

	return hr;
}


HRESULT STDMETHODCALLTYPE nxrmExt2::OnConnection(
	/*[in]*/ IDispatch * Application,
	/*[in]*/ AddInDesignerObjects::ext_ConnectMode ConnectMode,
	/*[in]*/ IDispatch * AddInInst,
	/*[in]*/ SAFEARRAY * * custom)
{
	
	//get application interface
	IDispatch* pAppObj = NULL;
	HRESULT	hr = E_FAIL;
	if (Application && (m_pAppObj == NULL))
	{
		do
		{
			hr = Application->QueryInterface(__uuidof(Word2016::_Application), (void**)&pAppObj);
			if (S_OK == hr)
			{
				m_OfficeAppType = OfficeAppWinword;
				m_pAppObj = pAppObj;

				for (const auto &ite : g_word_16_ribbon_info)
				{
					m_RibbonRightsMap.insert(std::pair<std::wstring, RIBBON_ID_INFO>(ite.RibbonId, ite));
				}
				Attache2Word();
				break;
			}

			hr = Application->QueryInterface(__uuidof(Excel2016::_Application), (void**)&pAppObj);
			if (S_OK == hr)
			{
				m_OfficeAppType = OfficeAppExcel;
				m_pAppObj = pAppObj;

				for (const auto &ite : g_excel_16_ribbon_info)
				{
					m_RibbonRightsMap.insert(std::pair<std::wstring, RIBBON_ID_INFO>(ite.RibbonId, ite));
				}
				Attache2Excel();
				break;
			}

			hr = Application->QueryInterface(__uuidof(PowerPoint2016::_Application), (void**)&pAppObj);
			if (S_OK == hr)
			{
				m_OfficeAppType = OfficeAppPowerpoint;
				m_pAppObj = pAppObj;

				for (const auto &ite : g_powerpoint_16_ribbon_info)
				{
					m_RibbonRightsMap.insert(std::pair<std::wstring, RIBBON_ID_INFO>(ite.RibbonId, ite));
				}
				Attache2Powerpoint();
				break;
			}


		} while (FALSE);

		//get RPM user
		SkyDrmSDKMgr::Instance()->GetCurrentLoggedInUser();
	}

	return S_OK;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnDisconnection(
	/*[in]*/ AddInDesignerObjects::ext_DisconnectMode RemoveMode,
	/*[in]*/ SAFEARRAY * * custom)
{
	HRESULT hr = S_OK;

	::OutputDebugStringW(L"nxrmExt2::OnDisconnection\n");
	return hr;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnAddInsUpdate(
	/*[in]*/ SAFEARRAY * * custom)
{
	HRESULT hr = S_OK;

	return hr;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnStartupComplete(
	/*[in]*/ SAFEARRAY * * custom)
{
	HRESULT hr = S_OK;

	return hr;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnBeginShutdown(
	/*[in]*/ SAFEARRAY * * custom)
{
	HRESULT hr = S_OK;

	return hr;
}

static BSTR LoadCustomUIFromFile(WCHAR *FileName)
{
	BSTR CustomUIXML = NULL;

	HANDLE hFile = INVALID_HANDLE_VALUE;

	BOOL bRet = TRUE;

	DWORD dwFileSize = 0;

	BYTE *buf = NULL;
	BYTE *p = NULL;
	DWORD bufLen = 0;

	DWORD BytesRead = 0;
	DWORD TotalBytesRead = 0;

	int BSTRLen = 0;

	do
	{
		hFile = CreateFileW(FileName,
			GENERIC_READ,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_SEQUENTIAL_SCAN,
			NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			break;
		}

		dwFileSize = GetFileSize(hFile, NULL);

		if (!dwFileSize)
		{
			break;
		}

		//
		// CustomUI.xml should not bigger than 1Mb
		//
		if (dwFileSize >= 1024 * 1024)
		{
			break;
		}

		bufLen = (dwFileSize + sizeof('\0') + 4095) & (~4095);

		buf = (BYTE*)malloc(bufLen);

		if (!buf)
		{
			break;
		}

		memset(buf, 0, bufLen);

		p = buf;
		TotalBytesRead = 0;

		do
		{
			bRet = ReadFile(hFile,
				(p - TotalBytesRead),
				(bufLen - TotalBytesRead),
				&BytesRead,
				NULL);

			if (!bRet)
			{
				break;
			}

			if (BytesRead == 0)
			{
				//
				// End of file
				//
				break;
			}

			TotalBytesRead += BytesRead;

		} while (TRUE);

		if (TotalBytesRead != dwFileSize)
		{
			break;
		}

		BSTRLen = MultiByteToWideChar(CP_UTF8,
			0,
			(LPCSTR)buf,
			TotalBytesRead,
			NULL,
			0);

		if (BSTRLen <= 0)
		{
			break;
		}

		CustomUIXML = SysAllocStringLen(NULL, BSTRLen);

		if (!CustomUIXML)
		{
			break;
		}

		BSTRLen = MultiByteToWideChar(CP_UTF8,
			0,
			(LPCSTR)buf,
			TotalBytesRead,
			CustomUIXML,
			BSTRLen);

		if (BSTRLen <= 0)
		{
			SysFreeString(CustomUIXML);
			CustomUIXML = NULL;
		}

	} while (FALSE);

	if (buf)
	{
		free(buf);
		buf = NULL;
	}

	if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
		hFile = INVALID_HANDLE_VALUE;
	}

	return CustomUIXML;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::GetCustomUI(BSTR RibbonID, BSTR * RibbonXml)
{
	WCHAR* wszRibbon = NULL;
	switch (m_OfficeAppType)
	{
	case OfficeAppPowerpoint:
		wszRibbon = POWERPNT_LAYOUT_XML_16;
		//wszRibbon = LoadCustomUIFromFile(L"C:\\Users\\administrator.QAPF1.000\\Desktop\\customUI.xml");
		break;

	case OfficeAppWinword:
		wszRibbon = WORD_LAYOUT_XML_16;
		//wszRibbon = LoadCustomUIFromFile(L"C:\\Users\\administrator.QAPF1.000\\Desktop\\customUI.xml");
		break;

	case OfficeAppExcel:
		wszRibbon = EXCEL_LAYOUT_XML_16;
		break;
	}

	//
	if (wszRibbon)
	{
		BSTR CustomUIXML = SysAllocString(wszRibbon);
		*RibbonXml = CustomUIXML;
	}

	return S_OK;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnCheckMsoButtonStatus(
	/*[in]*/ Office2016::IRibbonControl *pControl,
	/*[out, retval]*/ VARIANT_BOOL *pvarfEnabled)
{

	BSTR	ribbon_id = NULL;
	ULONGLONG uAcitveRight = BUILTIN_RIGHT_ALL;
	VARIANT_BOOL varEnable = VARIANT_TRUE;

	do 
	{
		/*
		//get right for acitve document
		if (m_OfficeAppType == OfficeAppExcel && m_pOfficeEventSink)
		{
			ExcelEventListener *pExcelEventSink = (ExcelEventListener *)m_pOfficeEventSink;

			pExcelEventSink->GetActiveRights(uAcitveRight);
		}
		else if (m_OfficeAppType == OfficeAppWinword && m_pOfficeEventSink)
		{
			WordEventListener *pWordEventSink = (WordEventListener*)m_pOfficeEventSink;

			pWordEventSink->GetActiveRights(uAcitveRight);
		}
		else if (m_OfficeAppType == OfficeAppPowerpoint && m_pOfficeEventSink)
		{
			PowerPointEventListener *pPowerPointEventSink = (PowerPointEventListener*)m_pOfficeEventSink;

			pPowerPointEventSink->GetActiveRights(uAcitveRight);
		}
		else
		{
			break;
		}
		*/
		if (!m_pOfficeEventSink)
		{
			break;
		}
		m_pOfficeEventSink->GetActiveRights(uAcitveRight);

		//check button status
		pControl->get_Id(&ribbon_id);
		const auto &ite = m_RibbonRightsMap.find(ribbon_id);

		if (ite != m_RibbonRightsMap.end())
		{
			varEnable = ((*ite).second.RightsMask & uAcitveRight) ? VARIANT_TRUE : VARIANT_FALSE;
		}

	} while (FALSE);
	

	*pvarfEnabled = varEnable;

	return S_OK;
}

HRESULT STDMETHODCALLTYPE nxrmExt2::OnLoad(IDispatch *RibbonUI)
{
	HRESULT hr = S_OK;

	m_pRibbonUI = RibbonUI;

	if (m_OfficeAppType == OfficeAppExcel)
	{
		InitExcelEventSink();
	}
	else if (m_OfficeAppType == OfficeAppPowerpoint)
	{
		InitPowerpointEventSink();
	}
	else if (m_OfficeAppType == OfficeAppWinword)
	{
		InitWordEventSink();
	}
	else
	{
	}

	HookManager::SetEventSink(m_pOfficeEventSink);

	return hr;
}

HRESULT nxrmExt2::Attache2Word(void)
{
	HRESULT hr = S_OK;

	IConnectionPointContainer	*pConnectionPointContainer = NULL;
	IConnectionPoint			*pConnectionPoint = NULL;
	DWORD                       dwAdviseCookie = 0;

	WordEventListener *pWordEventSink = NULL;

	do
	{
		if (m_OfficeAppType != OfficeAppWinword)
		{
			hr = E_UNEXPECTED;
			break;
		}

		try
		{
			pWordEventSink = new WordEventListener();
		}
		catch (std::bad_alloc exec)
		{
			pWordEventSink = NULL;
		}

		if (!pWordEventSink)
		{
			break;
		}

		hr = m_pAppObj->QueryInterface(IID_IConnectionPointContainer, (void**)&pConnectionPointContainer);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPointContainer->FindConnectionPoint(__uuidof(Word2016::ApplicationEvents4), &pConnectionPoint);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPoint->Advise(pWordEventSink, &dwAdviseCookie);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		m_pOfficeEventSink = pWordEventSink;

		pWordEventSink = NULL;

	} while (FALSE);

	if (pWordEventSink)
	{
		delete pWordEventSink;
		pWordEventSink = NULL;
	}

	if (pConnectionPoint)
	{
		pConnectionPoint->Release();
		pConnectionPoint = NULL;
	}

	if (pConnectionPointContainer)
	{
		pConnectionPointContainer->Release();
		pConnectionPointContainer = NULL;
	}

	return hr;
}

HRESULT nxrmExt2::Attache2Powerpoint(void)
{
	HRESULT hr = S_OK;

	IConnectionPointContainer	*pConnectionPointContainer = NULL;
	IConnectionPoint			*pConnectionPoint = NULL;
	DWORD                       dwAdviseCookie = 0;

	PowerPointEventListener *pPowerPointEventSink = NULL;
	ULONGLONG EvaluationId = 0;

	do
	{
		if (m_OfficeAppType != OfficeAppPowerpoint)
		{
			hr = E_UNEXPECTED;
			break;
		}

		try
		{
			pPowerPointEventSink = new PowerPointEventListener();
		}
		catch (std::bad_alloc exec)
		{
			pPowerPointEventSink = NULL;
		}

		if (!pPowerPointEventSink)
		{
			break;
		}

		hr = m_pAppObj->QueryInterface(IID_IConnectionPointContainer, (void**)&pConnectionPointContainer);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPointContainer->FindConnectionPoint(__uuidof(PowerPoint2016::EApplication), &pConnectionPoint);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPoint->Advise(pPowerPointEventSink, &dwAdviseCookie);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		m_pOfficeEventSink = pPowerPointEventSink;

		pPowerPointEventSink = NULL;

	} while (FALSE);

	if (pPowerPointEventSink)
	{
		delete pPowerPointEventSink;
		pPowerPointEventSink = NULL;
	}

	if (pConnectionPoint)
	{
		pConnectionPoint->Release();
		pConnectionPoint = NULL;
	}

	if (pConnectionPointContainer)
	{
		pConnectionPointContainer->Release();
		pConnectionPointContainer = NULL;
	}

	return hr;
}

HRESULT nxrmExt2::Attache2Excel(void)
{
	HRESULT hr = S_OK;

	IConnectionPointContainer	*pConnectionPointContainer = NULL;
	IConnectionPoint			*pConnectionPoint = NULL;
	DWORD                       dwAdviseCookie = 0;

	ExcelEventListener *pExcelEventSink = NULL;

	BSTR DocFullName = NULL;

	ULONGLONG RightsMask = BUILTIN_RIGHT_ALL;
	ULONGLONG CustomRights = 0;
	ULONGLONG EvaluationId = 0;

	do
	{
		if (m_OfficeAppType != OfficeAppExcel)
		{
			hr = E_UNEXPECTED;
			break;
		}

		try
		{
			pExcelEventSink = new ExcelEventListener();
		}
		catch (std::bad_alloc exec)
		{
			pExcelEventSink = NULL;
		}

		if (!pExcelEventSink)
		{
			break;
		}

		hr = m_pAppObj->QueryInterface(IID_IConnectionPointContainer, (void**)&pConnectionPointContainer);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPointContainer->FindConnectionPoint(__uuidof(Excel2016::AppEvents), &pConnectionPoint);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		hr = pConnectionPoint->Advise(pExcelEventSink, &dwAdviseCookie);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		m_pOfficeEventSink = pExcelEventSink;

		pExcelEventSink = NULL;

	} while (FALSE);

	if (DocFullName)
	{
		SysFreeString(DocFullName);
		DocFullName = NULL;
	}

	if (pExcelEventSink)
	{
		delete pExcelEventSink;
		pExcelEventSink = NULL;
	}

	if (pConnectionPoint)
	{
		pConnectionPoint->Release();
		pConnectionPoint = NULL;
	}

	if (pConnectionPointContainer)
	{
		pConnectionPointContainer->Release();
		pConnectionPointContainer = NULL;
	}

	return hr;
}

HRESULT nxrmExt2::InitWordEventSink(void)
{
	HRESULT hr = S_OK;

	ULONGLONG RightsMask = BUILTIN_RIGHT_ALL;
	ULONGLONG CustomRights = 0;
	ULONGLONG EvaluationId = 0;

	Word2016::_Application *pWinwordAppObj = NULL;
	Word2016::_Document *pDoc = NULL;
	BSTR DocFullName = NULL;

	do
	{
		if (m_OfficeAppType != OfficeAppWinword || m_pRibbonUI == NULL)
		{
			hr = E_UNEXPECTED;
			break;
		}
		pWinwordAppObj = (Word2016::_Application *)m_pAppObj;
		hr = pWinwordAppObj->get_ActiveDocument(&pDoc);
		if (!SUCCEEDED(hr))
		{
			break;
		}
		hr = pDoc->get_FullName(&DocFullName);
		if (!SUCCEEDED(hr))
		{
			break;
		}
		if (!DocFullName)
		{
			break;
		}
		hr = SkyDrmSDKMgr::Instance()->CheckRights(DocFullName, RightsMask, CustomRights);
		if (!SUCCEEDED(hr))
		{
			break;
		}
		m_pOfficeEventSink->Init(m_pRibbonUI, DocFullName, RightsMask);
	} while (FALSE);

	return hr;
}

HRESULT nxrmExt2::InitPowerpointEventSink(void)
{
	HRESULT hr = S_OK;
	ULONGLONG RightsMask = BUILTIN_RIGHT_ALL;
	ULONGLONG CustomRights = 0;
	PowerPoint2016::_Application *pPowerPointAppObj = NULL;
	PowerPoint2016::_Presentation *pPres = NULL;
	PowerPoint2016::ProtectedViewWindow *pProtectedWn = NULL;
	BSTR DocFullName = NULL;

	do
	{
		if (m_OfficeAppType != OfficeAppPowerpoint || m_pRibbonUI == NULL)
		{
			hr = E_UNEXPECTED;
			break;
		}
		pPowerPointAppObj = (PowerPoint2016::_Application *)m_pAppObj;

		hr = pPowerPointAppObj->get_ActivePresentation(&pPres);

		if (!SUCCEEDED(hr))
		{
			pPowerPointAppObj->get_ActiveProtectedViewWindow(&pProtectedWn);

			if (pProtectedWn)
			{

			}

			break;
		}

		hr = pPres->get_FullName(&DocFullName);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		if (!DocFullName)
		{
			break;
		}

		hr = SkyDrmSDKMgr::Instance()->CheckRights(DocFullName, RightsMask, CustomRights);

		if (!SUCCEEDED(hr))
		{
			break;
		}
		m_pOfficeEventSink->Init(m_pRibbonUI, DocFullName, RightsMask);
	} while (FALSE);

	if (pPres)
	{
		pPres->Release();
		pPres = NULL;
	}

	if (pProtectedWn)
	{
		pProtectedWn->Release();
		pProtectedWn = NULL;
	}

	return hr;
}

HRESULT nxrmExt2::InitExcelEventSink(void)
{
	HRESULT hr = S_OK;

	BSTR DocFullName = NULL;
	ULONGLONG RightsMask = BUILTIN_RIGHT_ALL;
	ULONGLONG CustomRights = 0;
	ULONGLONG EvaluationId = 0;
	Excel2016::_Application *pExcelAppObj = NULL;
	Excel2016::_Workbook	*pWb = NULL;
	Excel2016::ProtectedViewWindow *pProtectedWn = NULL;
	do
	{
		if (m_OfficeAppType != OfficeAppExcel || m_pRibbonUI == NULL)
		{
			hr = E_UNEXPECTED;
			break;
		}

		pExcelAppObj = (Excel2016::_Application *)m_pAppObj;

		hr = pExcelAppObj->get_ActiveWorkbook(&pWb);

		if (!SUCCEEDED(hr))
		{
			break;
		}

		if (pWb)
		{
			hr = pWb->get_FullName(0x0409/*US English*/, &DocFullName);

			if (!SUCCEEDED(hr))
			{
				break;
			}
		}
		else
		{
			hr = pExcelAppObj->get_ActiveProtectedViewWindow(&pProtectedWn);

			if (!SUCCEEDED(hr))
			{
				break;
			}

			//
			// TO DO: Adding code to deal with protected view
			//
			break;
		}

		if (!DocFullName)
		{
			break;
		}

		hr = SkyDrmSDKMgr::Instance()->CheckRights(DocFullName, RightsMask, CustomRights);

		if (!SUCCEEDED(hr))
		{
			break;
		}
		m_pOfficeEventSink->Init(m_pRibbonUI, DocFullName, RightsMask);
	} while (FALSE);

	if (pWb)
	{
		pWb->Release();
		pWb = NULL;
	}

	if (pProtectedWn)
	{
		pProtectedWn->Release();
		pProtectedWn = NULL;
	}

	return hr;
}