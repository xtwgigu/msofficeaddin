#include "stdafx.h"
#include "SkyDrmSDKMgr.h"
#include "log.h"
#include "rightsdef.h"


SkyDrmSDKMgr::SkyDrmSDKMgr()
{
	m_pRmcInstance = NULL;
	m_pRmTenant = NULL;
	m_pRmUser = NULL;
	m_strRMXPasscode = "{6829b159-b9bb-42fc-af19-4a6af3c9fcf6}";
	
}


SkyDrmSDKMgr::~SkyDrmSDKMgr()
{
}

bool SkyDrmSDKMgr::GetCurrentLoggedInUser()
{
	ISDRmcInstance *pInstance = NULL;
	ISDRmTenant *pTenant = NULL;
	ISDRmUser *puser = NULL;
	std::string strPasscode =  GetRMXPasscode();
	SDWLResult res = RPMGetCurrentLoggedInUser(strPasscode, pInstance, pTenant, puser);

	if (res.GetCode()==0){
		m_pRmcInstance = pInstance;
		m_pRmTenant = pTenant;
		m_pRmUser = puser;
	}

	//log
	theLog.WriteLog(0, NULL, 0, L"RPM RPMGetCurrentLoggedInUser res:%d, pInstance:%p, pTenant:%p, puser:%p\r\n",
		res.GetCode(), pInstance, pTenant, puser);


	

	return res.GetCode() == 0;
}

HRESULT SkyDrmSDKMgr::CheckRights(const WCHAR* wszFullFileName, ULONGLONG& RightMask, ULONGLONG& CustomRightsMask)
{
	//call sdk to get file right
	std::wstring wstrFile = wszFullFileName;
	VECTOR_RIGHTS rightsAndWatermarks;
	bool bGetRights = this->GetNxlFileRightsByRMInstance(wszFullFileName, rightsAndWatermarks);


#if 1
	//convert to right mask
	RightMask = BUILTIN_RIGHT_VIEW;
	if (bGetRights)
	{
		if (HaveRight(rightsAndWatermarks, RIGHT_EDIT))
		{
			RightMask |= BUILTIN_RIGHT_EDIT;
		}

		if (HaveRight(rightsAndWatermarks, RIGHT_PRINT))
		{
			RightMask |= BUILTIN_RIGHT_PRINT;
		}
	}
	


#else
	//test code for file right
	if ((wcschr(wszFullFileName, L'\\') == NULL) && (wcschr(wszFullFileName, L'/') == NULL))
	{
		RightMask = BUILTIN_RIGHT_ALL;
	}
	else
	{

		RightMask = BUILTIN_RIGHT_VIEW;

		if (wcsstr(wszFullFileName, L"write"))
		{
			RightMask |= BUILTIN_RIGHT_EDIT;
		}

		if (wcsstr(wszFullFileName, L"print"))
		{
			RightMask |= BUILTIN_RIGHT_PRINT;
		}
	}
#endif 

	return S_OK;
}

bool SkyDrmSDKMgr::GetNxlFileRightsByRMUser(const std::wstring& nxlfilepath, VECTOR_RIGHTS &rightsAndWatermarks)
{
	if (m_pRmUser==NULL){
		return false;
	}

	SDWLResult res = m_pRmUser->GetRights(nxlfilepath, rightsAndWatermarks);

	//log
	theLog.WriteLog(0, NULL, 0, L"GetNxlFileRightsByRMUser for:%s, res:%d, righs count:%d, sum rights:%d\r\n", 
		nxlfilepath.c_str(), res.GetCode(), rightsAndWatermarks.size(), this->SumRights(rightsAndWatermarks));

	return res.GetCode() == 0;
}

bool SkyDrmSDKMgr::GetNxlFileRightsByRMInstance(const std::wstring& plainFilePath, VECTOR_RIGHTS &rightsAndWatermarks)
{
	if (m_pRmcInstance==NULL)
	{
		return false;
	}

	SDWLResult res = m_pRmcInstance->RPMGetRights(plainFilePath, rightsAndWatermarks);

	//log
	theLog.WriteLog(0, NULL, 0, L"GetNxlFileRightsByRMInstance for plainFilePath:%s, res:%d, righs count:%d, sum rights:%d\r\n",
		plainFilePath.c_str(), res.GetCode(), rightsAndWatermarks.size(), this->SumRights(rightsAndWatermarks));

	return res.GetCode() == 0;
}

bool SkyDrmSDKMgr::HaveRight(const VECTOR_RIGHTS& vecRightAndWaterMark, SDRmFileRight right)
{
	VECTOR_RIGHTS::const_iterator itRights = vecRightAndWaterMark.begin();
	while (itRights != vecRightAndWaterMark.end())
	{
		if (itRights->first & right)
		{
			return true;
		}
		itRights++;
	}
	return false;
}

DWORD SkyDrmSDKMgr::SumRights(const VECTOR_RIGHTS& vecRights)
{
	DWORD dwRightsSum = 0;
	VECTOR_RIGHTS::const_iterator itRights = vecRights.begin();
	while (itRights != vecRights.end())
	{
		dwRightsSum += itRights->first;
		itRights++;
	}
	return dwRightsSum;
}

bool SkyDrmSDKMgr::EditSaveFile(const std::wstring &filepath, const std::wstring& originalNXLfilePath /* = L"" */, bool deletesource /* = false */, bool exitedit /* = false */)
{
	if (m_pRmcInstance==NULL)
	{
		return false;
	}

	SDWLResult res = m_pRmcInstance->RPMEditSaveFile(filepath, originalNXLfilePath, deletesource, exitedit);

	theLog.WriteLog(0, NULL, 0, L"RPMEditSaveFile path:%s, originPath:%s, deletesource:%d, exitedit:%d, res=%d\r\n",
		filepath.c_str(), originalNXLfilePath.c_str(), deletesource, exitedit, res.GetCode());


	return res.GetCode() == 0;
}

bool SkyDrmSDKMgr::EditCopyFile(const std::wstring &filepath, std::wstring& destpath)
{
	if (m_pRmcInstance == NULL)
	{
		return false;
	}

	SDWLResult res = m_pRmcInstance->RPMEditCopyFile(filepath, destpath);

	theLog.WriteLog(0, NULL, 0, L"RPMEditCopyFile path:%s, destpath:%s, res=%d\r\n",
		filepath.c_str(),destpath.c_str(), res.GetCode());


	return res.GetCode() == 0;
}