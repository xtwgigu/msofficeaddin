#pragma once
#include <Ole2.h>
#include "IEventBase.h"
namespace HookManager 
{
	typedef HRESULT(WINAPI *REGISTER_DRAG_DROP_FUN)(IN HWND hwnd, IN LPDROPTARGET pDropTarget);
	typedef HRESULT(WINAPI *DO_DRAG_DROP_FUN)(IN LPDATAOBJECT pDataObj, IN LPDROPSOURCE pDropSource, IN DWORD dwOKEffects, OUT LPDWORD pdwEffect);
	HRESULT WINAPI Core_RegisterDragDrop(IN HWND hwnd, IN LPDROPTARGET pDropTarget);
	HRESULT WINAPI Core_DoDragDrop(IN LPDATAOBJECT pDataObj, IN LPDROPSOURCE pDropSource, IN DWORD dwOKEffects, OUT LPDWORD pdwEffect);
	void hook();
	void unhook();
	void SetEventSink(IEventBase* pPights);
	class CoreIDropTarget : public IDropTarget
	{
	public:
		CoreIDropTarget();
		CoreIDropTarget(IDropTarget *pTarget);
		~CoreIDropTarget();

		STDMETHODIMP QueryInterface(
			/* [in] */ __RPC__in REFIID riid,
			/* [annotation][iid_is][out] */
			_COM_Outptr_  void **ppvObject);

		STDMETHODIMP_(ULONG) AddRef();

		STDMETHODIMP_(ULONG) Release();

		STDMETHODIMP DragEnter(
			/* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
			/* [in] */ DWORD grfKeyState,
			/* [in] */ POINTL pt,
			/* [out][in] */ __RPC__inout DWORD *pdwEffect);

		STDMETHODIMP DragOver(
			/* [in] */ DWORD grfKeyState,
			/* [in] */ POINTL pt,
			/* [out][in] */ __RPC__inout DWORD *pdwEffect);

		STDMETHODIMP DragLeave();

		STDMETHODIMP Drop(
			/* [unique][in] */ __RPC__in_opt IDataObject *pDataObj,
			/* [in] */ DWORD grfKeyState,
			/* [in] */ POINTL pt,
			/* [out][in] */ __RPC__inout DWORD *pdwEffect);

	private:
		ULONG			m_uRefCount;
		IDropTarget		*m_pIDropTarget;
	};
}